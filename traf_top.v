`timescale 1ns / 1ps

/*
* ������ �������� ������ ������ ���������.
* ��� ����������� ���������� �������, �� ������� ������� ������.
*/

module traf_top(
    input clk50,
    output green,
    output yellow,
    output red
    );

wire clk10hz;

traffic_div freq_div (
    .clk_in(clk50), 
    .clk_out(clk10hz)
    );
    
traffic_light traf_light (
    .clk(clk10hz), 
    .red(red), 
    .yellow(yellow), 
    .green(green)
    );

endmodule
